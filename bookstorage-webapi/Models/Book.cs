﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace bookstorage_webapi.Models
{
    public class Book
    {
        //common book id
        static int cid;

        public int Id {get; set;}
        public string Title { get; set; }
        public string Author { get; set; }

        public Book() { }

        public Book(Book book)
        {
            if (book.Id == 0)
            {
                this.Id = ++cid;
            }
            else this.Id = book.Id;
            this.Title = book.Title;
            this.Author = book.Author;
        }

        public Book(String Title, String Author)
        {
            this.Title = Title;
            this.Author = Author;
        }

        public Book(int id, String Title, String Author)
        {
            this.Id = id;
            this.Title = Title;
            this.Author = Author;
        }

    }
}