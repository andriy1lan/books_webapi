﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Diagnostics;
using Newtonsoft.Json;

using bookstorage_webapi.Models;
using bookstorage_webapi.Repositories;

namespace bookstorage_webapi.Controllers
{
    public class BooksController : ApiController
    {

        IRepository<Book> repo;
     // BookRepository repo = new BookRepository();

        public BooksController() {
          repo = new BookRepository();
        }

        public BooksController(IRepository<Book> repo)
        {
            this.repo = repo;
        }

        // GET api/books
        public HttpResponseMessage GetBooks()
        {
            IList<Book> books = repo.GetBooks();
            if (books.Count!= 0)
            {
                return Request.CreateResponse<IList<Book>>(HttpStatusCode.OK, books);
            }
            else { return Request.CreateErrorResponse(HttpStatusCode.NotFound, "No books found"); }
        }

        // GET api/books/5
        public HttpResponseMessage GetBook(int id)
        {
            Book book = repo.GetBook(id);
            if (book != null)
            {
                return Request.CreateResponse<Book>(HttpStatusCode.OK, book);
            }
            else { return Request.CreateErrorResponse(HttpStatusCode.NotFound, "No such book found"); }
        }

        // POST api/books
        public HttpResponseMessage PostBook([FromBody]Book book)
        {
            repo.AddBook(book);
            return Request.CreateResponse(HttpStatusCode.Created);
        }

        // PUT api/books/5
        public HttpResponseMessage PutBook([FromBody]Book book)
        {
            Book book0 = repo.UpdateBook(book);
            if (book0 != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            else { return Request.CreateErrorResponse(HttpStatusCode.NotFound, "No such book found"); }
        }

        // DELETE api/books/5
        public HttpResponseMessage DeleteBook(int id)
        {
            Book book = repo.DeleteBook(id);
            if (book != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            else { return Request.CreateErrorResponse(HttpStatusCode.NotFound, "No such book id found"); }

        }

      [NonAction]
      public void  DisableRepo() {
      repo.ClearRepo();
      }
    }
}
