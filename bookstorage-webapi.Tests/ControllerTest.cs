﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Hosting;

using bookstorage_webapi.Controllers;
using bookstorage_webapi.Models;
using bookstorage_webapi.Repositories;

using NUnit.Framework;
using Newtonsoft.Json;

namespace bookstorage_webapi.Tests
{
    [TestFixture]
    class ControllerTest
    {
        BookRepository repo;
        BooksController controller;

        [TestFixtureSetUp]
        public void Setup()
        {
            repo = new BookRepository();
            controller = new BooksController(repo);
            controller.Request = new HttpRequestMessage();
            var Configuration = new HttpConfiguration();
            controller.Request.Properties[HttpPropertyKeys.HttpConfigurationKey] = Configuration;
       
        }

        [Test]
        public void  Test_AddBook()
        {
            HttpResponseMessage response=controller.PostBook(new Book("Praporonosci", "Oles' Honchar"));
            Assert.AreEqual(response.StatusCode,HttpStatusCode.Created);
            response = controller.PostBook(new Book("Koni ne vynni", "Mykhaylo Kotyubynskyy"));
            Assert.AreEqual(response.StatusCode, HttpStatusCode.Created);
            string json = controller.GetBooks().Content.ReadAsStringAsync().Result;
            IList<Book> books = JsonConvert.DeserializeObject<IList<Book>>(json);
            Assert.AreEqual(books.Count, 2);
        }

        [Test]
        public void Test_GetAllBook()
        {
            string json = controller.GetBooks().Content.ReadAsStringAsync().Result;
            IList<Book> books = JsonConvert.DeserializeObject<IList<Book>>(json);
            Assert.AreEqual(books[0].Author, "Oles' Honchar");
            Assert.AreEqual(books.Count,1);
        }

        [Test]
        public void Test_GetBook()
        {
            string json = controller.GetBook(1).Content.ReadAsStringAsync().Result;
            Book book = JsonConvert.DeserializeObject<Book>(json);
            Assert.AreEqual(book.Title, "Praporonosci");
        }

        [Test]
        public void Test_UpdateBook()
        {
            controller.PutBook(new Book(1, "Sobor", "Oles' Honchar"));
            string json = controller.GetBook(1).Content.ReadAsStringAsync().Result;
            Book book = JsonConvert.DeserializeObject<Book>(json);
            Assert.AreEqual(book.Title, "Sobor");
           
        }

        [Test]
        public void Test_DeleteBook()
        {
            controller.DeleteBook(2);
            string json = controller.GetBooks().Content.ReadAsStringAsync().Result;
            IList<Book> books = JsonConvert.DeserializeObject<IList<Book>>(json);
            Assert.AreEqual(books.Count, 1);
        }

        [TestFixtureTearDown]
        public void close()
        {
           // controller.DisableRepo();
            //controller = null;
        }
    }
}
